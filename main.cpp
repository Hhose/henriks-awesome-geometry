// SPDX-License-Identifier: MIT
// Copyright (C) 2020 Henrik Hose

#include <iostream>
#include "lib/geometry.h"
#include <math.h>

int main() {

    std::string line = "============================================================================================\n";
    std::cout << line << "Default initializing, formatting with .to_string():\n" << std::endl;
    hag::Vec<double, 3>       vec;
    hag::Mat<double, 3,4>     mat;
    hag::RotMat<double>       S;
    hag::Quaternion<double>   q;
    hag::RotVec<double>       r;
    hag::Disp<double>         s;
    std::cout << "vec = " << vec.to_string() << std::endl;
    std::cout << "mat = " << mat.to_string() << std::endl;
    std::cout << "S =   " << S.to_string() << std::endl;
    std::cout << "r =   " << r.to_string() << std::endl;
    std::cout << "q =   " << q.to_string() << std::endl;
    std::cout << "s =   " << s.to_string() << std::endl << std::endl;





    std::cout << line << "Brace enclosed initializing, formatting with .display() for better readability in lon outputs: \n" << std::endl;
    hag::Vec<double, 3>       vecb{1.0, 2.0, 0.0}; // this fills up with zeros ;-)
    hag::Mat<double, 3,2>     matb{ 1.0, 2.0,
                                    3.0, 4.0,
                                    5.0, 6.0 };
    hag::RotMat<double>       Sb{1.0, 0.0, 0.0,
                                 0.0, 0.0, -1.0,
                                 0.0, 1.0, 0.0 };
    hag::RotVec<double>       rb{1.0, 2.0, 3.0};
    hag::Quaternion<double>   qb{0.707, 0.0, 0.0, 0.707};
    hag::Disp<double>         sb{1.0, 2.0, 3.0};
    std::cout << "vec = " << vecb.display() << std::endl;
    std::cout << "mat = " << matb.display() << std::endl;
    std::cout << "S =   " << Sb.display() << std::endl;
    std::cout << "r =   " << rb.display() << std::endl;
    std::cout << "q =   " << qb.display() << std::endl;
    std::cout << "s =   " << sb.display() << std::endl << std::endl;




    std::cout << line << "Initializing from std::array (row major interpretation), formatting with .display_vert() for vertical printing of vectors outputs:: \n" << std::endl;
    hag::Vec<double, 3>       vecarr(std::array<double,3>{0,3,6});
    hag::Mat<double, 3,2>     matarr( std::array<double,2*3>{0.0, 3.0, 6.0, 9.0, 12.0, 15.0 } );
    hag::RotMat<double>       Sarr(std::array<double,9>{1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0 });
    hag::RotVec<double>       rarr(std::array<double,3>{1.0, 2.0, 3.0});
    hag::Quaternion<double>   qarr(std::array<double,4>{0.707, 0.0, 0.0, 0.707});
    hag::Disp<double>         sarr(std::array<double, 3>{1.0, 2.0, 3.0});
    std::cout << "vec = " << vecarr.display_vert() << std::endl;
    std::cout << "mat = " << matarr.display() << std::endl;
    std::cout << "S =   " << Sarr.display() << std::endl;
    std::cout << "r =   " << rarr.display_vert() << std::endl;
    std::cout << "q =   " << qarr.display_vert() << std::endl;
    std::cout << "s =   " << sarr.display_vert() << std::endl << std::endl;




    std::cout << line << "Setting and getting individual elements (with size checking), note that normal std::array operations like std::swap still work: \n" << std::endl;
    vec.at(2) = 2.0;
    mat.at(1,2) = 3.0;
    mat.at(2*2) = 4.0;
    S.at(1,0) = 1.0; S.at(0,0) = 0.0;
    std::swap(S.at(1,0), S.at(0,1));
    r.at(0) = 0.0;
    qb.at(3) = -qb.at(3);
    std::cout << "vec(2) =     " << std::to_string(vec.at(2)) << std::endl;
    std::cout << "mat(1,2) =   " << std::to_string( mat.at(1,2) ) << std::endl;
    std::cout << "mat(2*2) =   " << std::to_string( mat.at(2*2) ) << std::endl;
    std::cout << "S =          " << S.display() << std::endl;
    std::cout << "r(0) =       " << std::to_string(r.at(0)) << std::endl;
    std::cout << "q =          " << qb.display() << std::endl;
    std::cout << "s =          " << s.display() << std::endl;
    try {
        std::cout << "Trying invalid access " << std::to_string(S.at(3,0)) << std::endl;
    }
    catch ( const std::out_of_range& e) {
        std::cout << "Out of Range error: " << e.what() << '\n';
    }



    std::cout << line << "Matrix multiplications: \n" << std::endl;
    hag::Vec<double,2> d{3.0, 1.0};
    hag::Mat<double,2,2> A{1.0, 2.0,       3.0, 4.0};
    hag::Mat<double,2,3> B{1.0, 2.0, 3.0,  4.0, 5.0, 6.0};
    hag::Mat<double,3,2> C{1.0, 2.0,       3.0, 4.0,    5.0, 6.0};
    auto res1 = A*d;
    auto res2 = A*B;
    auto res3 = B.transpose()*A;
    auto res4 = C*d;
    std::cout << "Mat A =    " << A.display() << std::endl;
    std::cout << "Mat B =    " << B.display() << std::endl;
    std::cout << "Vec d =    " << d.display_vert() << std::endl;
    std::cout << "Vec A*d  =" << res1.display_vert() << std::endl;
    std::cout << "Mat A*B  =" << res2.display() << std::endl;
    std::cout << "Mat B'*A =" << res3.display() << std::endl;
    std::cout << "Mat C*d  =" << res4.display() << std::endl;
    std::cout << "eye(3) =" << hag::Mat<double,3,3>::identity().display() << std::endl;
    std::cout << "eye(3,2) =" << hag::Mat<double,3,2>::identity().display() << std::endl<< std::endl;




    std::cout << line << "Rotation matrix: \n" << std::endl;
    hag::RotMat<double> rotx = hag::RotMat<double>::xRotMat(10.0 * M_PI / 180.0);
    hag::RotMat<double> roty = hag::RotMat<double>::yRotMat(20.0 * M_PI / 180.0);
    hag::RotMat<double> rotz = hag::RotMat<double>::zRotMat(30.0 * M_PI / 180.0);
    std::cout << "Rotation around x axis =    " << rotx.display() << std::endl;
    std::cout << "Rotation around y axis =    " << roty.display() << std::endl;
    std::cout << "Rotation around z axis =    " << rotz.display() << std::endl;
    hag::RotMat<double> rotzyx = rotz*roty*rotx;
    std::cout << "Rotation z-y-x (e.g. standard in DIN 9300; DIN ISO 8855; Tait-Bryan) =    " << rotzyx.display() << std::endl;
    hag::RotMat<double> rotzyx_mix = rotz*roty.toRotVec()*rotx.toQuat(); // yes, we can just do this and it rotates everything correctly!!! awsome!!!
    std::cout << "Rotation z-y-x from mixed multiplication = " << rotzyx_mix.display() << std::endl;
    std::cout << "Convert x rotation to quaternion      =    " << rotx.toQuat().to_string()   << std::endl;
    std::cout << "Convert x rotation to rotation vector =    " << rotx.toRotVec().to_string() << std::endl;
    std::cout << "Convert z-y-x rotation to quaternion  =    " << rotzyx.toQuat().to_string() << std::endl<< std::endl;



    std::cout << line << "Quaternion q = [w, x, y, z]: \n" << std::endl;
    hag::Quaternion<double> qx = rotx.toQuat();
    hag::Quaternion<double> qy = roty.toQuat();
    hag::Quaternion<double> qz = rotz.toQuat();
    hag::Quaternion<double> qeye;
    hag::Quaternion<double> qzyx = qz*qy*qx;
    qzyx.norm();
    hag::Quaternion<double> qzyx_mixed = qz*qy.toRotMat()*qx.toRotVec();
    std::cout << "Identity multiplication to quaternion  =    " << (qeye*qx).to_string() << std::endl;
    std::cout << "Quaternion multiplication z-y-x  =          " << qzyx.to_string() << std::endl;
    std::cout << "Quaternion z-y-x from mixed mult =          " << qzyx_mixed.to_string() << std::endl;
    std::cout << "Quaternion inverse z-y-x  =                 " << qzyx.inverse().to_string() << std::endl;
    std::cout << "Convert qx to rotation vector =             " << qx.toRotVec().to_string() << std::endl;
    std::cout << "Convert qx to rotation matrix =             " << qx.toRotMat().display() << std::endl;
    std::cout << "Access function qx.w() =                    " << std::to_string( qx.w()) << std::endl;
    std::cout << "Before swapping elements qx =               " << qx.to_string() << std::endl;
    std::swap( qx.w(), qx.z() );
    qx.x() = -qx.x();
    std::cout << "After swapping elements and invertig qx =   " << qx.to_string() << std::endl;



    std::cout << line << "Rotation Vector: rvec = angle * axis \n" << std::endl;
    hag::RotVec<double> rvec = qzyx.toRotVec();
    hag::RotVec<double> rvec_mixed = rotz.toRotVec()*roty*rotx.toQuat();
    std::cout << "rvec z-y-x  =                 " << rvec.to_string() << std::endl;
    std::cout << "rvec z-y-x multiplied =       " << (rotz.toRotVec()*roty.toRotVec()*rotx.toRotVec()).to_string() << std::endl;
    std::cout << "rvec z-y-x from mixed mult=   " << rvec_mixed.to_string() << std::endl;
    std::cout << "angle =                       " << std::to_string( rvec.angle() ) << std::endl;
    std::cout << "axis =                        " << (rvec.axis()).to_string() << std::endl;
    std::cout << "inverse rvec =                " << (rvec.inverse()).to_string() << std::endl;
    std::cout << "convert rvec to quaternion =  " << rvec.toQuat().to_string() << std::endl;
    std::cout << "convert rvec to rotation matrix = " << rvec.toRotMat().display() << std::endl;


    std::cout << line << "Rotate a vector: \n" << std::endl;
    hag::RotMat S_EB = rotzyx;                   // assume this rotates a vector in B frame to E frame
    hag::Quaternion<double> q_EB = rotzyx.toQuat();
    hag::RotVec rvec_EB = rotzyx.toRotVec();
    hag::Disp<double> r_EB_B{1.0, 2.0, 3.0};            // assume this is our displacement in B frame
    hag::Disp<double> r_EB_E = q_EB*r_EB_B;     // and rotates it to E frame
    hag::Vec<double, 3> w_EB_B{2.0, 3.0, 4.0};          // assume this is a rotational velocity in B frame
    hag::Vec<double, 3> w_EB_E = q_EB*w_EB_B;   // and rotates it to E frame
    std::cout << "Rotate vec by rotation matrix:  w_EB_E = S_EB*w_EB_B    = " << (S_EB*w_EB_B).to_string() << std::endl;
    std::cout << "Rotate vec by quaternion:       w_EB_E = q_EB*w_EB_B    = " << (q_EB*w_EB_B).to_string() << std::endl;
    std::cout << "Rotate vec by rotation vector:  w_EB_E = rvec_EB*w_EB_B = " << (rvec_EB*w_EB_B).to_string() << std::endl;
    std::cout << "Rotate disp by rotation matrix: r_EB_E = S_EB*r_EB_B    = " << (S_EB*r_EB_B).to_string() << std::endl;
    std::cout << "Rotate disp by quaternion:      r_EB_E = q_EB*r_EB_B    = " << (q_EB*r_EB_B).to_string() << std::endl;
    std::cout << "Rotate disp by rotation vector: r_EB_E = rvec_EB*r_EB_B = " << (rvec_EB*r_EB_B).to_string() << std::endl;

//    std::cout << line << "Householder QR decomposition: \n" << std::endl;
//    auto matA = hag::Mat<double, 3, 2>{1.0,1.0,   2.0,0.0,   2.0,0.0};
//    const auto resQR = hag::QRdecomposition<double, 3,2>(matA);
//    std::cout << "R = " << matA.getSubMat<1,2>(0,0).display() << std::endl;

    return 0;
}
