// SPDX-License-Identifier: MIT
// Copyright (C) 2020 Henrik Hose

#ifndef HENRIKS_AWESOME_GEOMETRY_ROTVEC_H
#define HENRIKS_AWESOME_GEOMETRY_ROTVEC_H

#include <array>
#include <complex>
#include "geometry.h"

namespace hag {
    /*
     * class holding rotvec as rvec = angle * n
     */
    template<class T>
    class RotVec : public Vec<T,3> {
    public:
        /*
         * default initializer to zeros
         */
        RotVec() = default;

        /*
         * intitializer from std::array
         */
        RotVec( const std::array<T,3>& l) : Vec<T,3>(l) {

        };

        /*
         * brace enclosed initializer
         */
        template<class... P>
        explicit RotVec(P&&... p) : Vec<T, 3>{std::forward<P>(p)...} {
            //static_assert(sizeof...(P) == 3, "Initializer list has wrong size ");
        }

        /*
         * convert to quaternion
         */
        Quaternion<T> toQuat() const {
            Quaternion<T> q;
            const auto angle = this->angle();
            const auto n = this->axis();
            q.at(0) = std::cos(this->angle()/2);
            q.at(1) = std::sin(this->angle()/2)*n.at(0);
            q.at(2) = std::sin(this->angle()/2)*n.at(1);
            q.at(3) = std::sin(this->angle()/2)*n.at(2);
            q.norm();
            return q;
        }

        /*
         * inverse()
         */
        RotVec<T> inverse() const {
            auto rvec = *this;
            rvec *=(-1);
            return rvec;
        }

        /*
         * convert to rotation matrix
         */
        RotMat<T> toRotMat() const {
            auto q = this->toQuat();
            return q.toRotMat();
        }

        /*
         * get angle of rotation
         */
        T angle() const {
            return norm(*this);
        }

        /*
         * get direction (axis)
         */
        Vec<T, 3> axis() const {
            if( this->angle() == 0 ) {
                return *this;
            }
            else {
                return (*this)/(this->angle());
            }
        }

        /*
         * multiplication of rotation with vector
         */
        Vec<T,3> operator*(const Vec<T,3>& vec) const {
            const auto& rvec = *this;
            return rvec.toRotMat()*vec;
        }

        /*
         * multiplication of rotation vector with rotation vector
         */
        RotVec<T> operator*(const RotVec<T>& rvec2) const {
            const auto& rvec1 = *this;
            Quaternion<T> qres = rvec1.toQuat()*rvec2.toQuat();
            return qres.toRotVec();
        }

        /*
         * multiplication of rotation vector with quaternion
         */
        RotVec<T> operator*(const Quaternion<T>& q) const {
            const auto& rvec = *this;
            Quaternion<T> qres = rvec.toQuat()*q;
            return qres.toRotVec();
        }

        /*
         * multiplication of rotation vector multiplied with rotation matrix
         */
        RotVec<T> operator*(const RotMat<T>& rotmat) const {
            const auto& rvec = *this;
            Quaternion<T> qres = rvec.toQuat()*rotmat.toQuat();
            return qres.toRotVec();
        }
    };
}

#endif //HENRIKS_AWSOME_GEOMETRY_ROTVEC_H
