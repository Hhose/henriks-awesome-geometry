// SPDX-License-Identifier: MIT
// Copyright (C) 2020 Henrik Hose

#ifndef HENRIKS_AWESOME_GEOMETRY_MAT_H
#define HENRIKS_AWESOME_GEOMETRY_MAT_H

#include <array>
#include <complex>
#include "geometry.h"

namespace hag {
    /*
    * matrix class for 2d matrices
    */
    template<class T, std::size_t ROWS, std::size_t COLS>
    class Mat : public std::array<T, ROWS * COLS> {
    public:
        /*
         * default initialize to zeros
         */
        Mat() : std::array<T, ROWS * COLS>{0} {
        }

        /*
         * default initialize to zeros
         */
        Mat(const std::array<T, ROWS * COLS> &l) : std::array<T, ROWS * COLS>(l) {
        }

        /*
         * brace enclosed initializer: can take up to SIZE arguments, fills up rest on zeros
         */
        template<class... P>
        explicit Mat(P &&... p) : std::array<T, ROWS * COLS>{std::forward<P>(p)...} {
            //static_assert(sizeof...(P) == ROWS*COLS, "Initializer list has wrong size");
        }

        /*
         * multiply aka vec = mat * vec
         */
        Vec <T, ROWS> operator*(const Vec <T, COLS> &vec) const {
            Vec <T, ROWS> res;
            const auto &mat = *this;
            for (std::size_t row = 0; row < ROWS; row++) {
                for (std::size_t col = 0; col < COLS; col++) {
                    res.at(row) += mat.at(COLS * row + col) * vec.at(col);
                }
            }
            return res;
        }

        /*
         * multiply aka matC = matA * matB
         */
        template<std::size_t OUTCOLS>
        Mat<T, ROWS, OUTCOLS> operator*(const Mat<T, COLS, OUTCOLS> &matB) const {
            Mat<T, ROWS, OUTCOLS> matC;
            const auto &matA = *this;
            for (size_t outrow = 0; outrow < ROWS; outrow++) {
                for (size_t outcol = 0; outcol < OUTCOLS; outcol++) {
                    for (size_t inner = 0; inner < COLS; inner++)
                        matC.at(outrow, outcol) += matA.at(outrow, inner) * matB.at(inner, outcol);
                }
            }
            return matC;
        }

        /*
         * scalar multiplication
         */
        Mat<T, ROWS, COLS> operator*(const T &t) const {
            const auto &mat = *this;
            Mat<T, ROWS, COLS> res;
            for (size_t i = 0; i < mat.size(); i++) {
                res.at(i) = mat.at(i) * t;
            }
            return res;
        }

        /*
         * scalar division
         */
        Mat<T, ROWS, COLS> operator/(const T &t) {
            const auto &mat = *this;
            Mat<T, ROWS, COLS> res;
            for (size_t i = 0; i < mat.size(); i++) {
                res.at(i) = mat.at(i) / t;
            }
            return res;
        }

        /*
         * multiply assign with scalar
         */
        void operator*=(const T &t) {
            for (auto &e : *this) {
                e *= t;
            }
        }

        /*
         * divide assign with scalar
         */
        void operator/=(const T &t) {
            for (auto &e : *this) {
                e /= t;
            }
        }

        /*
         * returns transpose
         */
        Mat<T, COLS, ROWS> transpose() {
            Mat<T, COLS, ROWS> trans;
            for (std::size_t row = 0; row < ROWS; row++) {
                for (std::size_t col = 0; col < COLS; col++) {
                    trans.at(col * ROWS + row) = this->at(row * COLS + col);
                }
            }
            return trans;
        }

        /*
         * identity
         */
        static Mat<T, ROWS, COLS> identity() {
            Mat<T, ROWS, COLS> mat;
            for (size_t row = 0; row < ROWS; row++) {
                for (size_t col = 0; col < COLS; col++) {
                    if (row == col) mat.at(row * COLS + col) = 1;
                }
            }
            return mat;
        }

        /*
         * to string
         */
        std::string to_string() {
            std::string s = "[ ";
            for (std::size_t row = 0; row < ROWS; row++) {
                s += "[ ";
                for (std::size_t col = 0; col < COLS; col++) {
                    s += std::to_string(this->at(row * COLS + col));
                    if (col < (COLS - 1)) s += ", ";
                }
                s += " ]";
                if (row < (ROWS - 1)) s += "\n";
            }
            s += " ]";
            return s;
        }

        /*
         * to string indented at new line
         */
        std::string display() {
            std::string s = "\n\t\t[ ";
            for (std::size_t row = 0; row < ROWS; row++) {
                s += "[ ";
                for (std::size_t col = 0; col < COLS; col++) {
                    s += std::to_string(this->at(row * COLS + col));
                    if (col < (COLS - 1)) s += ", ";
                }
                s += " ]";
                if (row < (ROWS - 1)) s += "\n \t\t  ";
            }
            s += " ]\n";
            return s;
        }

        /*
         * access operator for at(row, column)
         */
        using std::array<T, ROWS * COLS>::at;

        T &at(std::size_t row, std::size_t col) {
            if (row < ROWS && col < COLS) {
                return (this->at(row * COLS + col));
            } else {
                std::string err = "array::at: row, col (which is (" + std::to_string(row) + ", " + std::to_string(col)
                                  + ") >= ( " + std::to_string(ROWS) + ", " + std::to_string(COLS) + ") )\n";
                throw (std::out_of_range(err));
            }
        };

        constexpr const T &at(std::size_t row, std::size_t col) const {
            if (row < ROWS && col < COLS) {
                return (this->at(row * COLS + col));
            } else {
                std::string err = "array::at: row, col (which is (" + std::to_string(row) + ", " + std::to_string(col)
                                  + ") >= ( " + std::to_string(ROWS) + ", " + std::to_string(COLS) + " ) ) \n";
                throw (std::out_of_range(err));
            }
        };

        /*
         * access operator for specific column
         */
        Vec<T, ROWS> getCol(const std::size_t& col) const {
            if (col < COLS) {
                Vec<T, ROWS> res;
                for (std::size_t row = 0; row < ROWS; row++ ){
                    res.at(row) = this->at(row, col);
                }
                return res;
            } else {
                std::string err = "array::at: col (which is (" + std::to_string(col)
                                  + ") >= ( " + std::to_string(COLS) + ") )\n";
                throw (std::out_of_range(err));
            }
        }

        /*
         * access operator for specific row
         */
        Vec<T, COLS> getRow(const std::size_t& row) const {
            if (row < ROWS) {
                Vec<T, COLS> res;
                for (std::size_t col = 0; col < COLS; col++ ){
                    res.at(col) = this->at(row, col);
                }
                return res;
            } else {
                std::string err = "array::at: col (which is ("+ std::to_string(row)
                                                               + ") >= ( " + std::to_string(ROWS) + ") )\n";
                throw (std::out_of_range(err));
            }
        }

        /*
         * access operator for submatrix at(x, y, rows, cols);
         */
        template<std::size_t OUTROWS, std::size_t OUTCOLS>
        Mat<T, OUTROWS, OUTCOLS> getSubMat(const std::size_t& rowstart, const std::size_t& colstart) const {
            if (( rowstart + OUTROWS) <= ROWS && (colstart + OUTCOLS) <= COLS ){
                Mat<T, OUTROWS, OUTCOLS> submat;
                for ( std::size_t row = 0; row < OUTROWS; row++ ){
                    for ( std::size_t col = 0; col < OUTCOLS; col++ ){
                        submat.at(row,col) = this->at(row+rowstart, col+colstart);
                    }
                }
                return submat;
            }
            else {
                std::string err = "array::at: row, col (which is (" + std::to_string(rowstart) + ", " + std::to_string(colstart)
                                  + ") >= ( " + std::to_string(ROWS) + ", " + std::to_string(COLS) + ") )\n";
                throw (std::out_of_range(err));
            }
        }

        /*
         * access operator for specific column
         */
        void setCol(const std::size_t& col, const Vec<T, ROWS>& vec){
            if (col < COLS) {
                for (std::size_t row = 0; row < ROWS; row++ ){
                    this->at(row,col) = vec.at(row);
                }
            } else {
                std::string err = "array::at: col (which is (" + std::to_string(col)
                                  + ") >= ( " + std::to_string(COLS) + ") )\n";
                throw (std::out_of_range(err));
            }
        }

        /*
         * access operator for specific row
         */
        void setRow(const std::size_t& row, const Vec<T, COLS>& vec){
            if (row < ROWS) {
                for (std::size_t col = 0; col < COLS; col++ ){
                    this->at(row, col) = vec.at(col);
                }
            } else {
                std::string err = "array::at: col (which is ("+ std::to_string(row)
                                  + ") >= ( " + std::to_string(ROWS) + ") )\n";
                throw (std::out_of_range(err));
            }
        }

        /*
         * access operator for submatrix at(x, y, rows, cols);
         */
        template<std::size_t OUTROWS, std::size_t OUTCOLS>
        void setSubMat(const std::size_t& rowstart, const std::size_t& colstart, const Mat<T, OUTROWS, OUTCOLS>& mat){
            if (( rowstart + OUTROWS) < ROWS && (colstart + OUTCOLS) < COLS ){
                for ( std::size_t row = 0; row < OUTROWS; row++ ){
                    for ( std::size_t col = 0; col < OUTCOLS; col++ ){
                        this->at(row+rowstart, col+colstart) = mat.at(row, col);
                    }
                }
            }
            else {
                std::string err = "array::at: row, col (which is (" + std::to_string(rowstart) + ", " + std::to_string(colstart)
                                  + ") >= ( " + std::to_string(ROWS) + ", " + std::to_string(COLS) + ") )\n";
                throw (std::out_of_range(err));
            }
        }
    };

}

#endif //HENRIKS_AWSOME_GEOMETRY_MAT_H
