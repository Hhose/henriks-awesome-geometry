// SPDX-License-Identifier: MIT
// Copyright (C) 2020 Henrik Hose

#ifndef HENRIKS_AWESOME_GEOMETRY_ROTMAT_H
#define HENRIKS_AWESOME_GEOMETRY_ROTMAT_H

#include <array>
#include <complex>
#include "geometry.h"

namespace hag {

    /*
     * class for rotation matrix R(3,3)
     */
    template<class T>
    class RotMat : public Mat<T, 3, 3> {
    public:
        /*
         * standard constructor gives identity
         */
        RotMat() : Mat<T,3,3>(Mat<T,3,3>::identity()) {
        }

        /*
         * constructor from std::array
         */
        RotMat(const std::array<T,9>& l) : Mat<T,3,3>(l) {

        }

        /*
         * constructor from initializer list
         */
        template<class... P>
        explicit RotMat(P&&... p) : Mat<T, 3,3>{std::forward<P>(p)...} {
            //static_assert(sizeof...(P) == 9, "Initializer list has wrong size ");
        }

        /*
         * converts rotation matrix to quaternion q = [w, x*i, y*j, z*k]
         */
        Quaternion<T> toQuat() const {
            Quaternion<T> q;
            const auto& S = *this;
            T tr = S.at(0,0)+S.at(1,1)+S.at(2,2);
            if (tr > 0) {
                T sq = sqrt(tr+1.0) * 2; // S=4*qw
                q.at(0) = 0.25 * sq;
                q.at(1) = (S.at(2,1) - S.at(1,2)) / sq;
                q.at(2) = (S.at(0,2) - S.at(2,0)) / sq;
                q.at(3) = (S.at(1,0) - S.at(0,1)) / sq;
            } else if ((S.at(0,0) > S.at(1,1))&(S.at(0,0) > S.at(2,2))) {
                T sq = sqrt(1.0 + S.at(0,0) - S.at(1,1) - S.at(2,2)) * 2; // S=4*qx
                q.at(0) = (S.at(2,1) - S.at(1,2)) / sq;
                q.at(1) = 0.25 * sq;
                q.at(2) = (S.at(0,1) + S.at(1,0)) / sq;
                q.at(3) = (S.at(0,2) + S.at(2,0)) / sq;
            } else if (S.at(1,1) > S.at(2,2)) {
                T sq = sqrt(1.0 + S.at(1,1) - S.at(0,0) - S.at(2,2)) * 2; // S=4*qy
                q.at(0) = (S.at(0,2) - S.at(2,0)) / sq;
                q.at(1) = (S.at(0,1) + S.at(1,0)) / sq;
                q.at(2) = 0.25 * sq;
                q.at(3) = (S.at(1,2) + S.at(2,1)) / sq;
            } else {
                T sq = sqrt(1.0 + S.at(2,2) - S.at(0,0) - S.at(1,1)) * 2; // S=4*qz
                q.at(0) = (S.at(1,0) - S.at(0,1)) / sq;
                q.at(1) = (S.at(0,2) + S.at(2,0)) / sq;
                q.at(2) = (S.at(1,2) + S.at(2,1)) / sq;
                q.at(3) = 0.25 * sq;
            }
            q.norm();
            return q;
        }

        /*
         * rotation matrix to rotation vector
         */
        RotVec<T> toRotVec() const {
            auto q = this->toQuat();
            auto r = q.toRotVec();
            return r;
        }

        /*
         * assigns the inverse rotation to this
         */
        void invert() {
            *this = this->transpose();
        }

        /*
         * returns the inverse rotation
         */
        RotMat<T> inverse() {
            return this->transpose();
        }

        /*
         * returns rotation matrix around x axis for given angle
         */
        static RotMat<T> xRotMat(const T& ang){
            return RotMat{ 1.0, 0.0,            0.0,
                           0.0, std::cos(ang), -std::sin(ang),
                           0.0, std::sin(ang), std::cos(ang)};
        }


        /*
         * returns rotation matrix around y axis for given angle
         */
        static RotMat<T> yRotMat(const T& ang){
            return RotMat{ std::cos(ang),  0.0, std::sin(ang),
                           0.0,            1.0, 0.0,
                           -std::sin(ang), 0.0, std::cos(ang)};
        }

        /*
         * returns rotation matrix around z axis for given angle
         */
        static RotMat<T> zRotMat(const T& ang){
            return RotMat{ std::cos(ang), -std::sin(ang), 0.0,
                           std::sin(ang), std::cos(ang),  0.0,
                           0.0,             0.0,              1.0};
        }

        /*
         * multiplication rotation matrix with quaternion
         */
        RotMat<T> operator* (const Quaternion<T>& q) const {
            return ((*this)*q.toRotMat());
        }

        /*
         * multiplication rotation matrix with rotation vector
         */
        RotMat<T> operator* ( const RotVec<T>& rvec) const {
            return ((*this)*(rvec.toRotMat()));
        }

        /*
         * multiplication rotation matrix with rotation matrix
         */
        using Mat<T,3,3>::operator*;
        RotMat<T> operator* ( const RotMat<T>& rotmatB ) { // TODO: why cant i make this function const?!?!?!?!? --> Segfault
            const auto& rotmatA = *this;
            Mat<T,3,3> res = rotmatA*rotmatB; // this is const?!?
            return RotMat<T>(res);
        }
    };
}
#endif //HENRIKS_AWSOME_GEOMETRY_ROTMAT_H
