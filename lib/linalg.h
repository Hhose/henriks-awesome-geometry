//
// Created by hose on 05.04.20.
//

#ifndef HENRIKS_AWESOME_GEOMETRY_LINALG_H
#define HENRIKS_AWESOME_GEOMETRY_LINALG_H

#include "mat.h"

namespace hag{
    namespace ClassMethodLooper {
        template<int N, template<int I> class Wrapper, typename Parent, typename ... Args>
        struct Loop {
            static void impl(Parent *parent, const Args &... args) {
                //Build Wrapper struct.
                Wrapper<N> member = Wrapper<N>();

                //Call operator() of Wrapper
                member(parent, args...);

                //Continue Loop
                Loop<N - 1, Wrapper, Parent, Args ...>::impl(parent, args...);
            }
        };

        template<template<int I> class Wrapper, typename Parent, typename ... Args>
        struct Loop<0, Wrapper, Parent, Args ...> {
            static void impl(Parent *parent, const Args &... args) {
                //Do Nothing (terminate loop)
            }
        };
    }

//Call this to start the loop.
    template<int N, template<int I> class Wrapper, typename Parent, typename ... Args>
    void ClassMethodLoop(Parent *parent, const Args &... args) {
        ClassMethodLooper::Loop<N, Wrapper, Parent, Args ...>::impl(parent, args...);
    }

    template<class T, std::size_t ROWS, std::size_t COLS>
    class QRdecomposition {
    public:
        QRdecomposition(const Mat <T, ROWS, COLS> mat) : matA(mat), matQR(mat) {
            ClassMethodLoop<p - 1, householderDecompositionWrapper>(this);
        }

    private:
        const hag::Mat<T, ROWS, COLS> matA;
        hag::Mat<T, ROWS, COLS> matQR;
        static constexpr const size_t p = std::min(ROWS, COLS);
        hag::Vec<T, p> vecd;

        template<int N>
        struct householderDecompositionWrapper {
            void operator()(QRdecomposition *parent) const {
                parent->householderDecomposition<N>();
            }
        };

        template<int N>
        void householderDecomposition() {
            std::cout << N << std::endl;
            std::cout << ROWS << std::endl;
            std::cout << COLS << std::endl;
            std::cout << "matQR = " << matQR.getMat(0,0) << std::endl;
            Mat<T, ROWS-N, COLS-N> matQA;
            //this->matQR.getSubMat<ROWS-N, COLS-N>(N, N); // submatrix
//            std::cout << "QA pre transformation = " << matQA.display();
//            vecd.at(i) = householderTransformation(matQA); // householder transform
//            std::cout << "QA post transformation = " << matQA.display();
//            matQR.setSubMat<ROWS-i, COLS-i>(i,i, matQA);

        }

        T householderTransformation(Mat <T, ROWS, COLS> &A) {
            auto &y = A.getCol(0);
            T alpha = (y.at(0) >= 0 ? 1.0 : -1.0) * norm(y);
            auto v = y;
            y.at(0) += alpha;
            auto w = v * A;
            auto vTv = normSquared(v);
            A = A - 2.0 / vTv * dyadic(v, w);
            A.setCol(0, v);
            return -alpha;
        }
    };


//        /*
//         * return upper triangle form of decomposition
//         */
//        Mat<T, ROWS, COLS> getR() const {
//            Mat<T, ROWS, COLS> R;
//            for (std::size_t row = 0; row < p; row++) {
//                for (std::size_t col = row; col < p; col++) {
//                    if (row == col) {
//                        R.at(row, col) = d.at(row);
//                    } else {
//                        R.at(row, col) = QR.at(row, col);
//                    }
//                }
//            }
//            return R;
//        }

//        /*
//         * return orthogonal transformation matrix Q
//         */
//        Mat<T, ROWS, ROWS> getQ() const {
//            auto Q = Mat<T, ROWS, ROWS>::identity();
//            for (std::size_t i = 0; i < (p-1); i++ ) {
//                auto& Qi = Q.sub<ROWS-i, ROWS-i>(i,i);
//                const auto& vi = QR.sub<ROWS-i, COLS-i>(i,i).col(0);
//                Qi*=Mat<T,ROWS-i, ROWS-i>::identity() - 2.0/normSquared(vi) * dyadic(vi, vi);
//            }
//            return Q;
//        }
}

#endif //HENRIKS_AWESOME_GEOMETRY_LINALG_H
