// SPDX-License-Identifier: MIT
// Copyright (C) 2020 Henrik Hose

#ifndef HENRIKS_AWSOME_GEOMETRY_QUATERNION_H
#define HENRIKS_AWSOME_GEOMETRY_QUATERNION_H

#include <array>
#include <complex>
#include "geometry.h"

namespace hag {
    /*
     * class holding quaternion as q = [w, x, y, z]
     */
    template<class T>
    class Quaternion : public std::array<T, 4> {
    public:
        /*
         * standard constructor gives unit quaternion
         */
        Quaternion() : std::array<T, 4>{1, 0, 0, 0} {
            //norm();
        }

        /*
         * from std::array
         */
        Quaternion(const std::array<T, 4> &l) : std::array<T, 4>(l) {
            norm();
        }

        /*
         * brace enclosed initializer list
         */
        template<class... P>
        explicit Quaternion(P &&... p) : std::array<T, 4>{std::forward<P>(p)...} {
            //static_assert(sizeof...(P) == 4, "Initializer list has wrong size ");
            norm(); // norm so that we dont have to supply exact terms...
        }

        /*
         * constructor from rotation matrix
         */
        Quaternion(RotMat <T> S) {
            *this = S.toQuat();
        }

        /*
         * quaternion multiplication r = p x q
         * quaternion order is interpreted as: r = [ w,  x*i,  y*j,  z*k]
         */
        Quaternion<T> operator*(const Quaternion<T> &r) const {
            const auto &q = *this;
            Quaternion<T> t;
            t.at(0) = r.at(0) * q.at(0) - r.at(1) * q.at(1) - r.at(2) * q.at(2) - r.at(3) * q.at(3);
            t.at(1) = r.at(0) * q.at(1) + r.at(1) * q.at(0) - r.at(2) * q.at(3) + r.at(3) * q.at(2);
            t.at(2) = r.at(0) * q.at(2) + r.at(1) * q.at(3) + r.at(2) * q.at(0) - r.at(3) * q.at(1);
            t.at(3) = r.at(0) * q.at(3) - r.at(1) * q.at(2) + r.at(2) * q.at(1) + r.at(3) * q.at(0);
            return t;
        }

        /*
         * setter/getter for q = [ w,  x*i,  y*j,  z*k]
         */
        T &w() { return this->at(0); };

        T &x() { return this->at(1); };

        T &y() { return this->at(2); };

        T &z() { return this->at(3); };

        constexpr const T &w() const { return this->at(0); };

        constexpr const T &x() const { return this->at(1); };

        constexpr const T &y() const { return this->at(2); };

        constexpr const T &z() const { return this->at(3); };

        /*
         * multiplication quaternion with rotation vector
         */
        Quaternion<T> operator*(const RotVec <T> &r) const {
            const auto &q = *this;
            Quaternion<T> res = q * r.toQuat();
            return res;
        }

        /*
         * multiplication quaternion with rotation matrix
         */
        Quaternion<T> operator*(const RotMat <T> &r) const {
            const auto &q = *this;
            Quaternion<T> res = q * r.toQuat();
            return res;
        }

        /*
         * quaternion multiplication with vector
         */
        Vec<T, 3> operator*(const Vec<T, 3> &r) const {
            const auto &q = *this;
            Vec<T, 3> res = q.toRotMat() * r;
            return res;
        }

        /*
         * quaternion multiplication with vector
         */
        Disp <T> operator*(const Disp <T> &r) const {
            const auto &q = *this;
            Vec<T, 3> res = q.toRotMat() * r;
            return Disp<T>(res);
        }

        /*
         * quternion to rotation matrix
         * rotation matrix is row_major format
         * quaternion order is interpreted as: q = [ w,  x*i,  y*j,  z*k]
         */
        RotMat <T> toRotMat() const {
            RotMat <T> S;
            //this->norm();

            const auto &q = *this;

            // FUCK THIS WORKS
            T sqw = q.w()*q.w();
            T sqx = q.x()*q.x();
            T sqy = q.y()*q.y();
            T sqz = q.z()*q.z();

            T invs = 1 / (sqx + sqy + sqz + sqw);
            S.at(0,0) = ( sqx - sqy - sqz + sqw)*invs ; // since sqw + sqx + sqy + sqz =1/invs*invs
            S.at(1,1) = (-sqx + sqy - sqz + sqw)*invs ;
            S.at(2,2) = (-sqx - sqy + sqz + sqw)*invs ;

            T tmp1 = q.x()*q.y();
            T tmp2 = q.z()*q.w();
            S.at(1,0) = 2.0 * (tmp1 + tmp2)*invs ;
            S.at(0,1) = 2.0 * (tmp1 - tmp2)*invs ;

            tmp1 = q.x()*q.z();
            tmp2 = q.y()*q.w();
            S.at(2,0) = 2.0 * (tmp1 - tmp2)*invs ;
            S.at(0,2) = 2.0 * (tmp1 + tmp2)*invs ;
            tmp1 = q.y()*q.z();
            tmp2 = q.x()*q.w();
            S.at(2,1) = 2.0 * (tmp1 + tmp2)*invs ;
            S.at(1,2) = 2.0 * (tmp1 - tmp2)*invs ;

            // FUCK THIS DOESN'T WORK
//            S.at(0, 0) = 1.0 - 2.0*( q.at(2) * q.at(2) + q.at(3) * q.at(3) );
//            S.at(1, 0) = 2.0 * (q.at(1) * q.at(2) + q.at(0) * q.at(3));
//            S.at(2, 0) = 2.0 * (q.at(1) * q.at(3) - q.at(0) * q.at(2));
//
//            S.at(0, 1) = 2.0 * (q.at(1) * q.at(2) - q.at(0) * q.at(3));
//            S.at(1, 1) = 1.0 - 2.0 * ( q.at(1) * q.at(1) + q.at(3) * q.at(3));
//            S.at(2, 1) = 2.0 * (q.at(2) * q.at(3) + q.at(0) * q.at(1));
//
//            S.at(0, 2) = 2.0 * (q.at(1) * q.at(3) + q.at(0) * q.at(2));
//            S.at(1, 2) = 2.0 * (q.at(2) * q.at(3) - q.at(0) * q.at(1));
//            S.at(2, 2) = 1.0 - 2.0 * ( q.at(1) * q.at(1) + q.at(2) * q.at(2));

            return S;
        }

        /*
         * quaternion to rotation vector
         */
        RotVec <T> toRotVec() const {
            auto &q = *this;
            RotVec < T > r = std::array < T, 3 > {q.x(), q.y(), q.z()};
            auto norm_n = hag::norm(r); // sin(rho/2)
            T angle = std::atan2(norm_n, q.at(0)) * 2; // use atan2 because its usually the safest?
            if (norm_n != 0) {
                r /= norm_n;
                r *= angle;
            };
            return r;
        }

        /*
         * returns transpose
         */
        Quaternion<T> inverse() const {
            const auto &q = *this;
            Quaternion<T> qinv = q;
            for (size_t i = 1; i < 4; i++) {
                qinv.at(i) *= -1;
            }
            return qinv;
        }

        /*
         * euclidean norm of quaternion is set to 1
         */
        void norm() {
            T norm = 0;
            auto &q = *this;
            for (const auto &e : q) {
                norm += e * e;
            }
            q /= std::sqrt(norm);
        }

        /*
         * returns norm
         */
        T getNorm(){
            T norm = 0;
            auto &q = *this;
            for (const auto &e : q) {
                norm += e * e;
            }
            return norm;
        }

        /*
         * to string
         */
        std::string to_string() {
            std::string s = "[ ";
            for (std::size_t i = 0; i < this->size(); i++) {
                s += std::to_string(this->at(i));
                if (i < (this->size() - 1)) s += ", ";
            }
            s += " ]";
            return s;
        }

        /*
         * to string indented at new line
         */
        std::string display() {
            std::string s = "\n\t\t[ ";
            for (std::size_t i = 0; i < this->size(); i++) {
                s += std::to_string(this->at(i));
                if (i < (this->size() - 1)) s += ", ";
            }
            s += " ]\n";
            return s;
        }

        /*
        * to string indented at new line
        */
        std::string display_vert() {
            std::string s = "\n\t\t[ ";
            for (std::size_t i = 0; i < this->size(); i++) {
                s += "[ ";
                s += std::to_string(this->at(i));
                if (i < (this->size() - 1)) s += " ]\n\t\t  ";
            }
            s += " ] ]\n";
            return s;
        }

    private:
        /*
         * quaternion scalar multiplication, is not a valid quaternion operation therefore private
         */
        Quaternion<T> operator*(const T &t) {
            Quaternion q;
            for (std::size_t i = 0; i < 4; i++) {
                q.at(i) = this->at(i) * t;
            }
            return q;
        }

        /*
         * quaternion scalar division, is not a valid quaternion operation therefore private
         */
        Quaternion<T> operator/(const T &t) {
            Quaternion q;
            for (std::size_t i = 0; i < 4; i++) {
                q.at(i) = this->at(i) / t;
            }
            return q;
        }

        /*
         * quaternion scalar multiplication assignement
         */
        void operator*=(const T &t) {
            *this = (*this) * t;
        }

        /*
         * quaternion scalar division assignement
         */
        void operator/=(const T &t) {
            *this = (*this) / t;
        }
    };
}

#endif //HENRIKS_AWSOME_GEOMETRY_QUATERNION_H
