// SPDX-License-Identifier: MIT
// Copyright (C) 2020 Henrik Hose

#ifndef HENRIKS_AWESOME_GEOMETRY_IMU_H
#define HENRIKS_AWESOME_GEOMETRY_IMU_H

namespace hag {
template<class T>
class IMU {
public:
    explicit IMU(Quaternion<T> q_init = Quaternion<T>(), T Kp = T(0.4), T Ki = T(0.001)) :  q(q_init), Kp_(Kp), Ki_(Ki){
    }

    /**
     * @brief           update step for imu orientation estimation
     * @param gyr       gyroscope measurement in [rad]
     * @param acc       accelerometer measurement in [m/s^2]
     * @param dT        sampling time [s]
     * @param algorith  defaults to "mahony"
     * @return
     */
    Quaternion<T> update(Vec<T, 3> gyr, Vec<T,3> acc, const T& dT){
        // normalize the accelerometer
        if ( acc.norm() == 0 ) return q; // no devide by zero
        acc /= ( acc.norm() );
        // estimated direction of gravity
        Vec<T,3> half_v_est {   q.at(1) * q.at(3)   - q.at(0) * q.at(2),
                                q.at(0) * q.at(1)   + q.at(2) * q.at(3),
                                q.at(0) * q.at(0)   - T(0.5) + q.at(3)*q.at(3)};
        // error is cross product of estimated and measured gravity direction
        Vec<T, 3> half_err { acc.at(1) * half_v_est.at(2) - acc.at(2) * half_v_est.at(1), 
                          acc.at(2) * half_v_est.at(0) - acc.at(0) * half_v_est.at(2),
                          acc.at(0) * half_v_est.at(1) - acc.at(1) * half_v_est.at(0)};
        // integrate error term
        err_I += ( T(2.0) * Ki_ * dT * half_err );
        gyr += err_I;
        // proportional feedback
        gyr += T(2.0) * Kp_ * half_err;
        //integrate rate of change
        gyr *= ( dT * T(0.5));
        T qa = q.at(0);
        T qb = q.at(1);
        T qc = q.at(2);
        q.at(0) += ( -qb*gyr.at(0) - qc*gyr.at(1) - q.at(3)*gyr.at(2) );
        q.at(1) += (  qa*gyr.at(0) + qc*gyr.at(2) - q.at(3)*gyr.at(1) );
        q.at(2) += (  qa*gyr.at(1) - qb*gyr.at(2) + q.at(3)*gyr.at(0) );
        q.at(3) += (  qa*gyr.at(2) + qb*gyr.at(1) - qc * gyr.at(0) );

        q.norm();
        return q;
    }

    Quaternion<T> getOrientation() { return q;};

private:
    Quaternion<T> q{1., 0., 0., 0.};
    Vec<T,3> err_I;
    T Ki_;
    T Kp_;
    RotMat<double> S_SB{       0.0, -1.0, 0.0, // transformation that brings the body to the sensor frame by left multiplication
                                0.0,  0.0, 1.0,
                               -1.0,  0.0, 0.0 };
};
}

#endif //HENRIKS_AWESOME_GEOMETRY_IMU_H
