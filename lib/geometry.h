// SPDX-License-Identifier: MIT
// Copyright (C) 2020 Henrik Hose


#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <array>
#include <complex>

namespace hag {
    // forward declarations:
    template<class T, std::size_t SIZE> class Vec;
    template<class T, std::size_t ROWS, std::size_t COLS> class Mat;
    template<class T> class Disp;
    template<class T> class RotMat;
    template<class T> class Quaternion;
    template<class T> class RotVec;

    template<class T, std::size_t SIZE>
    T normSquared(const std::array<T, SIZE>& arr) {
        T normS = 0;
        for (auto& a : arr ) {
            normS += a*a;
        }
        return normS;
    }

    template<class T, std::size_t SIZE>
    T norm(const std::array<T, SIZE>& arr) {
        return std::sqrt(normSquared(arr));
    }

    template<class T, std::size_t SIZE>
    Mat<T, SIZE, SIZE> dyadic(const Vec<T,SIZE>& vec1, const Vec<T, SIZE>& vec2){
        Mat<T, SIZE, SIZE> mat;
        for (size_t row = 0; row <= SIZE; row++) {
            for (size_t col = 0; col <= SIZE; col++) {
                mat.at(row, col) = vec1.at(row)*vec2.at(col);
            }
        }
        return mat;
    }
}

#include "vec.h"
#include "mat.h"
#include "rotvec.h"
#include "rotmat.h"
#include "quaternion.h"
//#include "linalg.h"
#include "angrate.h"
#include "imu.h"

#endif //GEOMETRY_H