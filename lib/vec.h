// SPDX-License-Identifier: MIT
// Copyright (C) 2020 Henrik Hose

#ifndef HENRIKS_AWESOME_GEOMETRY_VEC_H
#define HENRIKS_AWESOME_GEOMETRY_VEC_H

#include <array>
#include <complex>
#include "geometry.h"

namespace hag {
    /*
     * class holding 1d vec
     */
    template<class T, std::size_t SIZE>
    class Vec : public std::array<T, SIZE> {
    public:
        Vec() : std::array<T, SIZE>{0} {
        };

        Vec(const std::array <T, SIZE> &l) : std::array<T, SIZE>(l) {
        }

        /*
         * brace enclosed initializer: can take up to SIZE arguments, fills up rest on zeros
         */
        template<class... P>
        explicit Vec(P &&... p) : std::array<T, SIZE>{std::forward<P>(p)...} {
            //static_assert(sizeof...(P) == SIZE, "Initializer list has wrong size");
        }

        /*
         * addition res = vecA + vecB
         */
        friend Vec<T, SIZE> operator+(const Vec<T, SIZE> &vecA, const Vec<T, SIZE> &vecB) {
            Vec<T, SIZE> res;
            for (std::size_t i = 0; i < SIZE; i++) {
                res.at(i) = vecA.at(i) + vecB.at(i);
            }
            return res;
        }

        /*
         * subtraction res = vecA - vecB
         */
        friend Vec<T, SIZE> operator-(const Vec<T, SIZE> &vecA, const Vec<T, SIZE> &vecB) {
            Vec<T, SIZE> res;
            for (std::size_t i = 0; i < SIZE; i++) {
                res.at(i) = vecA.at(i) - vecB.at(i);
            }
            return res;
        }

        /*
         * addition assignement vecA+= vecB
         */
        void operator+=(const Vec<T, SIZE> &vecB) {
            auto& vecA = *this;
            for (std::size_t i = 0; i< SIZE; i++) {
                vecA.at(i)+=vecB.at(i);
            }
        }

        /*
         * subtraction assignement vecA-= vecA
         */
        void operator-=(const Vec<T, SIZE> &vecB) {
            auto& vecA = *this;
            for (std::size_t i = 0; i< SIZE; i++) {
                vecA.at(i)-=vecB.at(i);
            }
        }

        /*
         * multiplication res = vec * mat
         */
        template<std::size_t COLS>
        friend Vec<T, COLS> operator*(const Vec<T, SIZE> &vec, Mat <T, SIZE, COLS> mat) {
            Vec<T, COLS> res;
            for (std::size_t col = 0; col < COLS; col++) {
                for (std::size_t row = 0; row < SIZE; row++) {
                    res.at(col) += vec.at(row) * mat.at(row * COLS + col);
                }
            }
            return res;
        }

        /*
         * scalar multiplication
         */
        friend Vec<T, SIZE> operator*(const Vec<T, SIZE> &vec, const T &t) {
            Vec<T, SIZE> res;
            for (size_t i = 0; i < SIZE; i++) {
                res.at(i) = vec.at(i) * t;
            }
            return res;
        }

        /*
         * scalar multiplication
         */
        friend Vec<T, SIZE> operator*(const T &t, const Vec<T, SIZE> &vec) {
            Vec<T, SIZE> res;
            for (size_t i = 0; i < SIZE; i++) {
                res.at(i) = vec.at(i) * t;
            }
            return res;
        }

        /*
         * scalar division
         */
        friend Vec<T, SIZE> operator/(const Vec<T, SIZE> &vec, const T &t) {
            Vec<T, SIZE> res;
            for (size_t i = 0; i < SIZE; i++) {
                res.at(i) = vec.at(i) / t;
            }
            return res;
        }

        /*
         * multiply assign with scalar
         */
        void operator*=(const T &t) {
            for (auto &e : *this) {
                e *= t;
            }
        }

        /*
         * divide assign with scalar
         */
        void operator/=(const T &t) {
            for (auto &e : *this) {
                e /= t;
            }
        }

        /*
         * returns vectors 2-norm
         */
        T norm( ) {
            T norm = 0;
            for ( auto &e : *this ){
                norm += e*e;
            }
            return std::sqrt(norm);
        }

        /*
         * to string
         */
        std::string to_string() {
            std::string s = "[ ";
            for (std::size_t i = 0; i < SIZE; i++) {
                s += std::to_string(this->at(i));
                if (i < (SIZE - 1)) s += ", ";
            }
            s += " ]";
            return s;
        }

        /*
         * to string indented at new line
         */
        std::string display() {
            std::string s = "\n\t\t[ ";
            for (std::size_t i = 0; i < SIZE; i++) {
                s += std::to_string(this->at(i));
                if (i < (SIZE - 1)) s += ", ";
            }
            s += " ]\n";
            return s;
        }

        /*
         * to string indented at new line
         */
        std::string display_vert() {
            std::string s = "\n\t\t[ ";
            for (std::size_t i = 0; i < SIZE; i++) {
                s += "[ ";
                s += std::to_string(this->at(i));
                if (i < (SIZE - 1)) s += " ]\n\t\t  ";
            }
            s += " ] ]\n";
            return s;
        }

    };


/*
 * class holding displacement vector
 */
    template<class T>
    class Disp : public Vec<T, 3> {
    public:
        /*
         * default to zero
         */
        Disp() = default;

        /*
         * from std::array
         */
        Disp(const std::array<T, 3> &l) : Vec<T, 3>(l) {}

        /*
         * from brace enclosed initializer
         */
        template<class... P>
        explicit Disp(P &&... p) : Vec<T, 3>{std::forward<P>(p)...} {
            //static_assert(sizeof...(P) == 3, "Initializer list has wrong size ");
        }

    };


    /**
     * @brief compute the skew symmetric form of a Vec<T,3>
     * @return
     */
    template<class T>
    Mat<T,3,3> skew(Vec<T,3> vec){
        Mat<T,3,3>  mat;
        mat.at(0,1) = -vec.at(2);
        mat.at(0,2) = vec.at(1);
        mat.at(1,2) = -vec.at(0);

        mat.at(1, 0) = mat.at(0,1);
        mat.at(2, 0) = mat.at(0,2);
        mat.at(2, 1) = mat.at(1,2);
        return mat;
    }
}

#endif