//
// Created by hose on 21.04.20.
//

#ifndef HENRIKS_AWESOME_GEOMETRY_ANGRATE_H
#define HENRIKS_AWESOME_GEOMETRY_ANGRATE_H

namespace hag {
    template<class T>
    class AngRate : public Vec<T, 3> {
    public:
        AngRate() : Vec<T,3>{0} {
        };

        AngRate(const std::array <T, 3> &l) : Vec<T,3>(l) {
        };

        /*
         * brace enclosed initializer: can take up to SIZE arguments, fills up rest on zeros
         */
        template<class... P>
        explicit AngRate(P &&... p) : Vec<T,3>{std::forward<P>(p)...} {
            //static_assert(sizeof...(P) == SIZE, "Initializer list has wrong size");
        }

        /**
         * @brief multiplication of angular rate with quaternion. Ment for use as: dq/dt = 1/2 * omega * q;
         * @param q
         * @return omega * q
         */
        Quaternion<T> operator*(const Quaternion<T> &q) const {
            Quaternion<T> Omega;
            Omega.at(0) = T(0);
            Omega.at(1) = this->at(0);
            Omega.at(2) = this->at(1);
            Omega.at(3) = this->at(2);
            return Omega*q;
        }

        /**
         * scalar multiplication
         */
        friend AngRate<T> operator*(const AngRate<T> &vec, const T &t) {
            AngRate<T> res;
            for (size_t i = 0; i < 3; i++) {
                res.at(i) = vec.at(i) * t;
            }
            return res;
        }

        /**
         * scalar multiplication
         */
        friend AngRate<T> operator*(const T &t, const AngRate<T> &vec) {
            AngRate<T> res;
            for (size_t i = 0; i < 3; i++) {
                res.at(i) = vec.at(i) * t;
            }
            return res;
        }


//        using Vec<T,3>::operator*;
//        using Vec<T,3>::operator+;
//        using Vec<T,3>::operator/;
//        using Vec<T,3>::operator-;
        using Vec<T,3>::operator*=;
        using Vec<T,3>::operator+=;
        using Vec<T,3>::operator/=;
        using Vec<T,3>::operator-=;
    };
}

#endif //HENRIKS_AWESOME_GEOMETRY_ANGRATE_H
