# Henriks Awesome Geometry
Header only geometry library that is derived from std::array

Uses quaternion, rotation vector and rotation matrix to rotate vectors or matrices.

All data is stored row-major, such that solver (e.g. qpOASES), Matlab-generated c-code, etc. can easily be interfaced by calling the inherited `std::array::data()`